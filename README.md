This was the first project I completed while interning at Paradynamix. I was tasked with creating a calculator that would perform basic operations, with more complicated tasks such as parentheses being added later in the project. The program was created using Windows Presentation Form (WPF) and C#. I had experience working with C++, but I had never worked with any of the languages or frameworks needed for this project.

This calculator is capable of full basic functionality including addition, subtraction, multiplication, and division. It will also handle negative numbers and parenthesized expressions. Any incorrect input, either via the built in buttons or with a keyboard will be met with an error message explaining why the input is invalid.

Goals:
1. Fully functioning basic calculator
2. Keyboard input
3. Partially functioning scientific calculator
4. polished ui