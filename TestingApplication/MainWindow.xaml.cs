using System;
using System.Windows;
using System.Windows.Controls;

namespace TestingApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary

    //TODO
    //Keyboard input
    //History

    public partial class MainWindow : Window
    {
        public MainWindow() => InitializeComponent();

        //Counter for parentheses
        int counter = 0;

        /// <summary>
        /// Adds input to Display if input is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Viewbox box = (Viewbox)(sender as Button).Content;
            TextBlock block = (TextBlock)box.Child;
            string input = block.Text;
            string Display = (string)DisplayBox.Content;
            bool isLastInputOperator = false;
            bool isCurrentInputOperator = false;

            ///Checks if display is in valid state for input && input is valid given current display
            //Clears non numerical results
            if(Display == "NaN" || Display == "∞")
            {
                Clear(sender, e);
                Display = "";
            }
            //Prevents operator as first input
            if ((Display == null || Display == "") && (input[0] == '+' || input[0] == '*' || input[0] == '/' || input[0] == '.'))
            {
                MessageBox.Show("Cannot enter operator as first expression");
            }
            //Inserts first numerical value or parentheses
            else if (Display == null || Display == "")
            {
                DisplayBox.Content += block.Text;
            }
            //Checks for illegal inputs and shows an error if found
            else
            {
                isCurrentInputOperator = IsOperator(input, 0);
                isLastInputOperator = IsOperator(Display, Display.Length - 1);
                //Prevents parentheses to be used in place of '*'
                if (!isCurrentInputOperator && Display[Display.Length - 1] == ')')
                {
                    MessageBox.Show("Parentheses do not support implicit multiplication(yet), please enter an operator");
                }
                //Prevents operators from being placed after '(' without a preceding number
                else if (Display[Display.Length - 1] == '(' && isCurrentInputOperator && input[0] != '-')
                {
                        MessageBox.Show("Please enter number after '('");
                }
                else if (isCurrentInputOperator == true && (isLastInputOperator == true))
                {
                    //Prevents operator after minus before any numerical input
                    if (Display[0] == '-' && Display.Length == 1 && input[0] == '-')
                    {
                        MessageBox.Show("Cannot enter minus negative as first expression");
                    }
                    //Allows '-' to be used as a negative
                    else if (input[0] == '-' && (isLastInputOperator || Display[Display.Length - 1] == '(') && Display.Length >= 2 && !IsOperator(Display,Display.Length-2))
                    {
                        DisplayBox.Content += block.Text;
                    }
                    //Prevents double operator
                    else
                    {
                        MessageBox.Show("Cannot enter multiple operators in sequence");
                    }
                }
                //Inserts valid input to display
                else
                {
                    DisplayBox.Content += block.Text;
                }
            }
        }

        /// <summary>
        /// Determines if specified character in a string is an operator and returns result
        /// </summary>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private static bool IsOperator(string data, int offset)
        {
            if (data[offset] == '+' || data[offset] == '-' || data[offset] == '*' || data[offset] == '/' || data[offset] == '.')
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Clears Display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Clear(object sender, RoutedEventArgs e)
        {
            DisplayBox.Content = "";
            counter = 0;
        }

        /// <summary>
        /// Removes last item from display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackSpace(object sender, RoutedEventArgs e)
        {
            string str = (string)DisplayBox.Content;
            if (!string.IsNullOrEmpty(str))
            {
                if(str[str.Length-1] == '(')
                {
                    counter--;
                }
                else if(str[str.Length-1] == ')')
                {
                    counter++;
                }
                str = str.Remove(str.Length - 1);
                DisplayBox.Content = str;
                return;
            }
        }

        /// <summary>
        /// Calculates results of given display based on order of operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Calculate(object sender, RoutedEventArgs e)
        {
            string str = (string)DisplayBox.Content;
            string temp = "";
            double[] op = new double[100];
            char[] operand = new char[100];
            bool isSolved = false;
            bool isRightFound = false;
            bool isLeftFound = false;
            int i = 0;
            int j = 0;
            int leftParenthesesindex = 0;
            int rightParenthesesindex = 0;

            //Prevents calculation of nothing
            if (str == null || str == "")
            {
                MessageBox.Show("Input cannot be blank");
            }
            //Prevents trailing operator
            else if (IsOperator(str,str.Length - 1))
            {
                MessageBox.Show("Last input must be a number");
            }
            //Prevents non matching parentheses
            else if (counter != 0)
            {
                MessageBox.Show("All left parentheses must have a matching right parentheses");
            }
            //Calculates result
            else
            {
                //Dirty workaround for previous result being shown in scientific notation
                for (int k = 0; k < str.Length; k++)
                {
                    if(str[k] == 'E')
                    {
                        MessageBox.Show("Cannot calculate with 'E+'(yet), please clear result box before calculation");
                        return;
                    }
                }
                //Loops through current display until all operators are removed
                while (!isSolved)
                {
                    j = 0;
                    i = 0;
                    isSolved = true;
                    isRightFound = false;
                    isLeftFound = false;

                    //Checks for right parentheses
                    while (!isRightFound && j < str.Length)
                    {
                        if (str[j] == ')')
                        {
                            rightParenthesesindex = j;
                            isRightFound = true;
                            isSolved = false;
                        }
                        else
                        {
                            j++;
                        }
                    }
                    //Checks for left parentheses if right parentheses is found
                    while (!isLeftFound && isRightFound)
                    {
                        if (str[j] == '(')
                        {
                            leftParenthesesindex = j;
                            isLeftFound = true;
                        }
                        else
                        {
                            j--;
                        }

                    }
                    
                    if (isLeftFound)
                    {
                        temp = str.Substring(leftParenthesesindex + 1, rightParenthesesindex - (leftParenthesesindex + 1));
                    }
                    else
                    {
                        temp = str;
                    }

                    CreateOpArrays(ref temp, op, operand, ref i);

                    OperatorMath(op, operand, ref i);
                    if(isLeftFound)
                    {
                        temp = str.Substring(0, leftParenthesesindex);
                        temp += Convert.ToString(op[0]);
                        temp += str.Substring(rightParenthesesindex + 1);
                        str = temp;
                    }
                }
                DisplayBox.Content = Convert.ToString(op[0]);
            }
        }

        /// <summary>
        /// Creates two arrays with operators and operands from given string using i as the index of the arrays
        /// </summary>
        /// <param name="str"></param>
        /// <param name="op"></param>
        /// <param name="operand"></param>
        /// <param name="i"></param>
        private static void CreateOpArrays(ref string str, double[] op, char[] operand, ref int i)
        {
            string temp = "";
            bool isNegative = false;
            while (str.Length > 0)
            {
                if (str[0] == '+' || str[0] == '-' || str[0] == '*' || str[0] == '/')
                {

                    if (str[0] == '-' && temp == "")
                    {
                        isNegative = !isNegative;
                    }
                    else if( str[0] == '(' || str[0] == ')')
                    {
                        i++;
                    }
                    else
                    {
                        op[i] = Convert.ToDouble(temp);
                        operand[i] = str[0];
                        i++;
                        temp = "";
                        if (isNegative)
                        {
                            op[i - 1] *= -1;
                            isNegative = false;
                        }
                    }
                }
                else
                {
                    temp += str[0];
                }
                str = str.Substring(1);
            }

            if (temp != "")
            {
                op[i] = Convert.ToDouble(temp);
                if (isNegative)
                    op[i] *= -1;
            }
        }

        /// <summary>
        /// Calculates answer using arrays of operators and operands until index i is reached
        /// </summary>
        /// <param name="op"></param>
        /// <param name="operand"></param>
        /// <param name="i"></param>
        private static void OperatorMath(double[] op, char[] operand, ref int i)
        {
            int k = 0;
            bool isSolved = false;
            //Multiplication and Division
            while (!isSolved)
            {
                if (k >= i)
                {
                    isSolved = true;
                }
                else if (operand[k] == '/')
                {
                    op[k] = op[k] / op[k + 1];
                    AlignOps(op, operand, ref i, k);
                }
                else if (operand[k] == '*')
                {
                    op[k] = op[k] * op[k + 1];
                    AlignOps(op, operand, ref i, k);
                }
                else
                {
                    k++;
                }
            }
            isSolved = false;
            k = 0;

            // Addition and Subtraction
            while (!isSolved)
            {
                if (k >= i)
                {
                    isSolved = true;
                }
                else if (operand[k] == '-')
                {
                    op[k] = op[k] - op[k + 1];
                    AlignOps(op, operand, ref i, k);
                }
                else if (operand[k] == '+')
                {
                    op[k] = op[k] + op[k + 1];
                    AlignOps(op, operand, ref i, k);
                }
                else
                {
                    k++;
                }
            }
        }

        /// <summary>
        /// Removes blank space in arrays and used values
        /// </summary>
        /// <param name="op"></param>
        /// <param name="operand"></param>
        /// <param name="i"></param>
        /// <param name="k"></param>
        private static void AlignOps(double[] op, char[] operand, ref int i, int k)
        {
            while (k < i)
            {
                op[k + 1] = op[k + 2];
                operand[k] = operand[k + 1];
                k++;
            }
            i--;
        }

        /// <summary>
        /// Adds parentheses to display if valid and keeps track of # of parentheses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Parentheses(object sender, RoutedEventArgs e)
        {
            Viewbox box = (Viewbox)(sender as Button).Content;
            TextBlock block = (TextBlock)box.Child;
            string input = block.Text;
            string Display = (string)DisplayBox.Content;

            if (input[0] == '(')
            {
                if (Display == null || Display == "")
                {
                    DisplayBox.Content += block.Text;
                    counter++;
                }
                else if (IsOperator(Display, Display.Length - 1))
                {
                    DisplayBox.Content += block.Text;
                    counter++;
                }
                else if( Display[Display.Length-1] == '(')
                {
                    DisplayBox.Content += block.Text;
                    counter++;
                }
                else
                {
                    MessageBox.Show("Parentheses do not support implicit multiplication(yet), please enter an operator");
                }

            }
            else
            {
                if (Display == null || Display == "")
                {
                    MessageBox.Show("Expression cannot start with right parentheses");
                }
                else if (IsOperator(Display, Display.Length - 1))
                {
                    MessageBox.Show("Right parentheses must follow a complete expression");
                }
                else if (counter < 1)
                {
                    MessageBox.Show("Right parentheses must follow a left parentheses");
                }
                else if ( Display[Display.Length-1] == '(')
                {
                    MessageBox.Show("Parentheses cannot be empty");
                }
                else
                {
                    DisplayBox.Content += block.Text;
                    counter--;
                }
            }
        }
    }
}